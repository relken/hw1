import java.util.Arrays;

public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      Color test[] = new Color[] { Color.green, Color.blue, Color.green, Color.blue, Color.red,
              Color.green, Color.green, Color.green, Color.red, Color.green,
              Color.red, Color.red, Color.red, Color.blue };

      Color testBalls[] = new Color[20];

      for (int i=0; i < testBalls.length; i++) {
         double rnd = Math.random();
         if (rnd < 1./3.) {
            testBalls[i] = Color.red;
         } else  if (rnd > 2./3.) {
            testBalls[i] = Color.blue;
         } else {
            testBalls[i] = Color.green;
         }
      }
      System.out.println(Arrays.toString(testBalls));
      reorder(testBalls);
      System.out.println(Arrays.toString(testBalls));
   }
   
   public static void reorder (Color[] balls) {
      int redCount= 0;
      int greenCount= 0;
      for (Color ball : balls)
         if (ball == Color.red) redCount++;
         else if (ball == Color.green) greenCount++;

      int i, j, z;
      for (i = 0; i < redCount; i++)
         balls[i] = Color.red;
      for (j = i; j < redCount + greenCount; j++)
         balls[j] = Color.green;
      for (z = j; z < balls.length; z++)
         balls[z] = Color.blue;
   }
}

